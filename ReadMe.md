Domain Widget
=============

Known Issues
------------
- The source in the branch `bad_dependencies/graphs`, which starts from , starting from commit 78e5acac71b9ec86287b821fa66bc1777292cfa1,
 will not be successfully compiled on its own
    - The branch is used in the project `topocule` as a git submodule
    - Dependencies to code in that project was introduced accidentally
    - The new code parts is planned to be moved out from this Domain Widget project into the topocule project
