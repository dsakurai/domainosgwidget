//
// Created by SakuraiDaisuke on 9/25/16.
//

#include "DomainOSGWidget.h"

#include <CameraLogic.h>
#include <vis_util_osg.h>

#include <osg/Material>
#include <osg/MatrixTransform>
#include <osgGA/StandardManipulator>
#include <osgDB/WriteFile>
#include <osgText/Text>
#include <QWindow>

#include "FSPickHandler.h"

namespace {
    bool isCameraOrtho(osg::Camera *camera) {
        double left, right, bottom, top, zNear, zFar;
        return camera->getProjectionMatrixAsOrtho(left, right, bottom, top, zNear, zFar);
    }
}

DomainOSGWidget::DomainOSGWidget(QWidget *parent, Qt::WindowFlags f) :
        OSGWidget (parent, f, /*demo = */ false),
        camera_selection_overlay (new osg::Camera),
        projectionMatrixDelegate{*this},
        picker (new FSPickHandler)
{
    auto scene_data = dynamic_cast<osg::Group*>(getMainView()->getSceneData());
    if (!scene_data){
        std::cerr <<"error: could not get scene data node as a group node.\t"<< "\x1b[37m" << __FILE__ << ":" << __LINE__ << "\x1b[0m" << std::endl;
        return;
    }

    auto viewer_ = getViewer();
    auto view = getMainView();

    osg::Camera* camera_main = getCameraOfMainView();
    camera_main->setGraphicsContext(getGraphicsWindow());

    // hide polygons for selection
    camera_main->setCullMask(CameraLogic::MAIN);
#ifdef OVERLAY
    camera_selection_overlay->setCullMask(CameraLogic::SELECTION_OVERLAY);

    camera_selection_overlay->setName("camera_selection_overlay");

    {
        camera_selection_overlay->setViewport(0, 0, this->width(), this->height());
        camera_selection_overlay->setClearColor(osg::Vec4(1.f, 0.f, 0.0f, 1.f));
        camera_selection_overlay->setProjectionMatrix(osg::Matrix::ortho2D(
                0, width(), 0, height()
        ));
    }

    /// @todo projection matrix needs update after resizing the window
    camera_selection_overlay->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
    camera_selection_overlay->setClearMask(GL_DEPTH_BUFFER_BIT); // clear depth for overlay rendering
    camera_selection_overlay->setRenderOrder(osg::Camera::POST_RENDER);
    camera_selection_overlay->setAllowEventFocus(false);

    camera_selection_overlay->setGraphicsContext(getGraphicsWindow());

    // add selection rect
    {
        camera_selection_overlay->addChild(picker->getOrCreateSelectionRect());
    }

    {
        osg::ref_ptr<osg::StateSet> overlay_state = camera_selection_overlay->getOrCreateStateSet();
        overlay_state->setMode(GL_BLEND, osg::StateAttribute::ON);
    }

    camera_selection_overlay->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    scene_data->addChild(camera_selection_overlay);
#endif // OVERLAY

    { // OpenGL states
        auto stateSet = scene_data->getOrCreateStateSet();

//            osg::Material *material = new osg::Material;
//            material->setColorMode(osg::Material::AMBIENT_AND_DIFFUSE);
//            stateSet->setAttributeAndModes(material, osg::StateAttribute::ON);

        stateSet->setMode(GL_DEPTH_TEST, osg::StateAttribute::ON);
    }

//    setCameraMask(camera_selection_overlay, CameraLogic::SELECTION_OVERLAY);

//    view->addEventHandler( new osgViewer::StatsHandler );

    auto manipulator = getStandardManipulatorOfMainView();
    if (!manipulator) {
        throw std::runtime_error {"failed to get the camera manipulator as StandardManipulator"};
    }
    manipulator->setAllowThrow(false);
    manipulator->setAutoComputeHomePosition(true);

//    view->addEventHandler( new osgViewer::StatsHandler );
    view->addEventHandler(picker);

#ifdef NEW

    {




        osg::ref_ptr<osg::Geode> textGeode = new osg::Geode;
        std::string str = "Dummy";
        osg::ref_ptr<osgText::Text> text = new osgText::Text;
        osg::ref_ptr<osgText::Font> font = osgText::readFontFile("fonts/arial.ttf");
        text->setFont(font.get());
        text->setCharacterSize(32.f);
        text->setAxisAlignment(osgText::TextBase::XY_PLANE);
//        text->setPosition(osg::Vec3(256., 256., 0.));
        text->setPosition(osg::Vec3(width() / 2.0, height() / 2.0, 0.));
        text->setText(str);
        textGeode->addDrawable(text);

#ifdef TEXT
        camera_selection_overlay->addChild(textGeode);
#endif // TEXT
    }

    view->setCamera( camera_main );
    view->setSceneData( scene_data );
    view->addEventHandler( new osgViewer::StatsHandler );
//    view->addEventHandler( new FSPickHandler );

    osg::ref_ptr<osgGA::TrackballManipulator> manipulator = new osgGA::TrackballManipulator;
    manipulator->setAllowThrow( false );
    manipulator->setAutoComputeHomePosition(true);

    view->setCameraManipulator( manipulator );

    // selection box to be rendered
//    scene_data->addChild( picker->getOrCreateSelectionBox() );
    view->addEventHandler(picker);


    viewer_->addView(view);

    viewer_->setThreadingModel(osgViewer::ViewerBase::SingleThreaded );
    viewer_->realize();

#ifdef DEBUG_OSG
    osgDB::writeNodeFile( *viewer_->getView(0)->getSceneData(),
                          "/Users/daisuke/sceneGraph.osg" );
#endif // DEBUG_OSG
    
    // This ensures that the widget will receive keyboard events. This focus
    // policy is not set by default. The default, Qt::NoFocus, will result in
    // keyboard events that are ignored.
    setFocusPolicy(Qt::ClickFocus);
    setMinimumSize(100, 100 );

#endif // NEW
    // Ensures that the widget receives mouse move events even though no
    // mouse button has been pressed. We require this in order to let the
    // graphics window switch viewports properly.
//  this->setMouseTracking( true );
}

bool DomainOSGWidget::event( QEvent* event ) {
    return OSGWidget::event(event);
}

osg::Camera* DomainOSGWidget::getCameraOfMainView() {
    return getMainView()->getCamera();
}

void DomainOSGWidget::resizeGL(int width, int height) {
    OSGWidget::resizeGL(width, height);

    // @todo test this
    auto pixelRatio = this->devicePixelRatio();
    std::vector<osg::Camera*> cameras;
    getViewer()->getCameras( cameras );

    for (int c = 0; c < cameras.size(); ++c) {
        auto& camera = cameras[c];
        camera->setViewport( 0, 0, width * pixelRatio, height * pixelRatio );

        // Currently we only care about camera 0, which is the camera of the main view
        if (c == 0 && isCameraOrtho(camera)) {
            projectionMatrixDelegate.setProjectionMatrixAsOrtho2D();
            projectionMatrixDelegate.setProjectionMatrixAsOrtho2D();
        }
    }

    camera_selection_overlay->setProjectionMatrix(osg::Matrix::ortho2D(
            0, width * pixelRatio, 0, height * pixelRatio
    ));
}

/**
 * The way we update the OSG scene data may be expensive.
 * If this becomes the bottleneck, consult
 * Chapter 8 Animating Scene Objects (p. 193)
 * of "OpenSceneGraph Beginner's Guide"
 * for a possible speed gain.
 */
void DomainOSGWidget::updateSceneGraphTraversal() {

        // @todo Possible bottleneck
        getViewer()->updateTraversal();

        update();
}

void DomainOSGWidget::resetCameraPosition() {
    getViewer()->getView(0)->home();
}

void DomainOSGWidget::setBoundingBox(double *bounds, osg::ref_ptr<osg::Group> sceneData) {
    using namespace std;

    osg::ref_ptr<osg::Geometry> boundingBox = vis_util_osg::createNewBox(bounds);
    sceneData->addChild(boundingBox);

    cerr <<"Currently supports only Mac font directory \t"<< "\x1b[37m" << __FILE__ << ":" << __LINE__ << "\x1b[0m" << std::endl;
    
    osg::ref_ptr<osgText::Text> textX = new osgText::Text;
    osg::ref_ptr<osgText::Text> textY = new osgText::Text;
    osg::ref_ptr<osgText::Text> textZ = new osgText::Text;
    osg::ref_ptr<osgText::Text> textO = new osgText::Text;
    
    auto setupText = [bounds, sceneData](osg::ref_ptr<osgText::Text> text){

        // Mac
        float charactersize = 2.0f;
        std::string font          = "/Library/Fonts/Arial.ttf"; // for macOS
#ifdef __linux__ // poor man's way to adjust settings for linux
        charactersize = 0.1f;
        font = "arial.ttf";
#endif

        text->setFont(font);
        text->setCharacterSize(charactersize);
        text->setAxisAlignment(osgText::Text::SCREEN);
        text->setFontResolution(100,100);

        sceneData->addChild(text);
    };
    
    setupText(textX);
    setupText(textY);
    setupText(textZ);
    setupText(textO);

    textO->setAlignment(osgText::Text::RIGHT_TOP);
    textX->setAlignment(osgText::Text::LEFT_BOTTOM);
    textY->setAlignment(osgText::Text::LEFT_BOTTOM);
    textZ->setAlignment(osgText::Text::LEFT_BOTTOM);
    
    textO->setPosition(osg::Vec3(bounds[0], bounds[2], bounds[4]));
    textX->setPosition(osg::Vec3(bounds[1], bounds[2], bounds[4]));
    textY->setPosition(osg::Vec3(bounds[0], bounds[3], bounds[4]));
    textZ->setPosition(osg::Vec3(bounds[0], bounds[2], bounds[5]));
    
    textX->setText("X");
    textY->setText("Y");
    textZ->setText("Z");
    textO->setText("O");
}

void DomainOSGWidget::focusInEvent(QFocusEvent *event) {

    emit focusIn();
    QWidget::focusInEvent(event);
}

FSPickHandler *DomainOSGWidget::getPicker() const {
    return picker;
}

void DomainOSGWidget::ProjectionMatrixDelegate::setUpOrthoGraphicCamera(
        const std::array<double,3>& center,
        double rect_width, double rect_height
) {
    // set camera center
    auto manipulator = widget.getStandardManipulatorOfMainView();
    manipulator->setAutoComputeHomePosition(false);
    manipulator->setHomePosition(
            osg::Vec3d{center[0], center[1], center[2] + 1.0},
            osg::Vec3d{center[0], center[1], center[2]},
            /*up*/ {0.0, 1.0, 0.0}
    );

    // set the bounds
    _ortho2DParameters = {.left = -0.5 * rect_width, .right = 0.5 * rect_width, .bottom = -0.5 * rect_height, .top = 0.5 * rect_height};
    // Use the parameters
    setProjectionMatrixAsOrtho2D();
}

osgGA::StandardManipulator *DomainOSGWidget::getStandardManipulatorOfMainView() {
    auto view = getMainView();
    if (!view) {
        return nullptr;
    }

    return dynamic_cast<osgGA::StandardManipulator*>(view->getCameraManipulator());
}

void DomainOSGWidget::ProjectionMatrixDelegate::setProjectionMatrixAsOrtho2D() {
    double aspectRatio = (double) widget.width() / (double)widget.height();
    widget.getCameraOfMainView()->setProjectionMatrixAsOrtho2D(
            _ortho2DParameters.left * aspectRatio,
            _ortho2DParameters.right * aspectRatio,
            _ortho2DParameters.bottom,
            _ortho2DParameters.top);
}
