//
// Created by SakuraiDaisuke on 9/25/16.
//

#ifndef BACKEND_DOMAINOSGWIDGET_H
#define BACKEND_DOMAINOSGWIDGET_H

#include <OSGWidget.h>

#include <array>
#include <memory>

class FSPickHandler;

namespace osgGA {
    class StandardManipulator;
}

class DomainOSGWidget : public OSGWidget {
Q_OBJECT
protected:
    virtual void focusInEvent(QFocusEvent *event) override;

public:
    DomainOSGWidget(QWidget *parent = 0, Qt::WindowFlags f = Qt::Widget);
    void resetCameraPosition();

    /// get the camera used for rendering in the view
    osg::Camera* getCameraOfMainView();

    osgGA::StandardManipulator* getStandardManipulatorOfMainView();

    /** Rather high-level management of the projection matrix
     * because OpenSceneGraph's projection matrix is a bit low-level.
     */
    class ProjectionMatrixDelegate {
    public:
        ProjectionMatrixDelegate(DomainOSGWidget& widget): widget {widget} {}

        /// Parameters for the orthographic projection
        struct Ortho2DParameters {
            double left = 0.0;
            double right = 1.0;
            double bottom = 0.0;
            double top = 1.0;
        };

        /**
         * Look at the x-y plane in the camera.
         * The view corresponds to the scenery of rectangular region having the specified width and height in the world
         * coordinate. The coordinates are that of the view space, which is subject to the camera position and
         * orientation, but the length unit is identical to the world coordinate unless a very weird view matrix is
         * set (which we won't let happen).
         *
         * From the given parameters, this function computes the projection matrix and pass it to the renderer.
         * This function calls `setProjectionMatrixAsOrtho2D` internally.
         *
         * @param center: center of the viewed scenery
         * @param rect_width: rectangular width of the view
         * @param rect_height:rectangular height of the view
         */
        void setUpOrthoGraphicCamera(const std::array<double, 3>& center, double rect_width, double rect_height);

        /**
         * Pass the projection matrix for the orthographic projection to OpenSceneGraph.
         */
        void setProjectionMatrixAsOrtho2D();

    private:
        DomainOSGWidget& widget;
        Ortho2DParameters _ortho2DParameters;
    };

    using Ortho2DParameters = ProjectionMatrixDelegate::Ortho2DParameters;

protected:
    bool event( QEvent* event ) override;
    void resizeGL(int width, int height) override;

    /**
     * Set the bounding box of the domain.
     * This box is set to be visible for the main camera;
     * but not for the selection camera.
     * @param bounds
     */
    void setBoundingBox(double *bounds, osg::ref_ptr<osg::Group> sceneData);
    FSPickHandler *getPicker() const;

    ProjectionMatrixDelegate* getCameraDelegate(){
        return &projectionMatrixDelegate;
    }

public slots:
    void updateSceneGraphTraversal();
signals:
    void focusIn();

private:
    // overlay camera for selection
    osg::ref_ptr<osg::Camera> camera_selection_overlay;

    /// Manage the camera (currently only the camera 0, which is the 3D camera of the view)
    ProjectionMatrixDelegate projectionMatrixDelegate;

    FSPickHandler* picker = NULL;
};


#endif //BACKEND_DOMAINOSGWIDGET_H
