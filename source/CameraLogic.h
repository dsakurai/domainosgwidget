//
// Created by SakuraiDaisuke on 9/30/16.
//

#ifndef BACKEND_CAMERALOGIC_H
#define BACKEND_CAMERALOGIC_H


class CameraLogic {
public:
    enum CameraMask {
        MAIN       = 0b01,
        SELECTION_OVERLAY    = 0b10,
//        ANY_CAMERA = 0b11
    };
};

#endif //BACKEND_CAMERALOGIC_H
