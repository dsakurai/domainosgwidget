//
// Created by Daisuke Sakurai on 2/23/19.
//

#ifndef DOMAINWIDGET_MAINWINDOW_H
#define DOMAINWIDGET_MAINWINDOW_H

#include <QMainWindow>
#include <string>

class Ui_MainWindow;

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow (QWidget* parent = NULL);
    ~MainWindow();

    void addMesh(std::string filename);
private:
    Ui_MainWindow * ui;
};


#endif //DOMAINWIDGET_MAINWINDOW_H
