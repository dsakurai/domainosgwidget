//
// Created by SakuraiDaisuke on 9/26/16.
//

#ifndef BACKEND_FSPICKHANDLER_H
#define BACKEND_FSPICKHANDLER_H

#include <array>

#include <osgGA/GUIEventHandler>
#include <osg/Material>


class QCheckBox;

class FSPickHandler : public osgGA::GUIEventHandler
{
public:
    FSPickHandler();

    osg::Node* getOrCreateSelectionBox();
    osg::ref_ptr<osg::Node> getOrCreateSelectionRect();
#ifdef BUFFER_OVERFLOW
    void setSelectionModel(SelectionModel* fsSelection);
#endif // BUFFER_OVERFLOW

    /** Add selection
     * 
     * @todo Selection should go into the temporal selection stack.
     *  
     * Currently, the intersection test picks even meshes that are completely occluded.
     * To pick those only visible to the user, we could:
     * 1. do line-intersection test for every pixel in the selection rect; or
     * 2. implement an OpenGL-based off-screen picker.
     * 
     * @param ea 
     * @param aa 
     * @return 
     */
    virtual bool handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa );

protected:
    void updateRect(float x, float y);

    /**
     * @todo Making use of line degeneracy is unintuitive and may lead to bug.
     * Rather find an option to hide something in OSG.
     */
    void endSelection();

    osg::ref_ptr<osg::Geometry> _selectionRect;

    /**
     * true: add picked objects
     * false: remove picked objects
     */
    QCheckBox * selectionModeCheckBox = NULL;
public:
    void setSelectionModeCheckBox(QCheckBox *selectionModeCheckBox);

protected:

    std::array<float, 2> _selectionOrigin;
//    osgViewer::

private:
    osg::ref_ptr<osg::StateSet> selected_state = new osg::StateSet;
    osg::ref_ptr<osg::Material> selected_material = new osg::Material;
};


#endif //BACKEND_FSPICKHANDLER_H
