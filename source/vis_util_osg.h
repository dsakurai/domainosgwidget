//
// Created by SakuraiDaisuke on 8/19/16.
//

#ifndef BACKEND_VIS_UTIL_OSG_H
#define BACKEND_VIS_UTIL_OSG_H

#include <osg/Geometry>
#include <memory>
#include <ostream>

#include <cassert>
#include <string>

namespace vis_util_osg {

    osg::Vec4 setColor(int c);
    osg::ref_ptr<osg::Geometry> createNewBox(double * bound);

    std::string serialize(osg::Node& node);
};

std::ostream& operator<< (std::ostream& o, osg::Node& node);

#endif //BACKEND_VIS_UTIL_OSG_H
