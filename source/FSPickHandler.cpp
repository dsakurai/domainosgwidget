//
// Created by SakuraiDaisuke on 9/26/16.
//

#include "FSPickHandler.h"

#include <vis_util_osg.h>

#include <iostream>

#include <osg/Geode>
#include <osg/Material>
#include <osg/MatrixTransform>
#include <osg/PolygonMode>
#include <osg/ShapeDrawable>
#include <osgText/Font>
#include <osgText/Text>
#include <osgUtil/PolytopeIntersector>
#include <osgViewer/Viewer>

#include <QGuiApplication>
#include <QCheckBox>

osg::ref_ptr<osg::Node> FSPickHandler::getOrCreateSelectionRect() {
    // create selection rect
    if(!_selectionRect){
        // viewport coordinate 
        osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array;
        vertices->push_back(osg::Vec3(0.f, 0.f, 0.f));
        vertices->push_back(osg::Vec3(0.f, 0.f, 0.f));
        vertices->push_back(osg::Vec3(0.f, 0.f, 0.f));
        vertices->push_back(osg::Vec3(0.f, 0.f, 0.f));

        _selectionRect = new osg::Geometry;
        _selectionRect->setName("selection_rect");
        _selectionRect->setVertexArray(vertices);
        
        osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
        colors->push_back(osg::Vec4(0.7f, 0.7, 0.7, 0.3f));

        _selectionRect->setColorArray(colors);
        _selectionRect->setColorBinding(osg::Geometry::BIND_OVERALL);
        _selectionRect->addPrimitiveSet(new osg::DrawArrays(GL_LINE_LOOP, 0, 4));
        osg::StateSet*ss = _selectionRect->getOrCreateStateSet();
        ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
        ss->setMode(GL_BLEND, osg::StateAttribute::ON);
    }
    return _selectionRect.get();
}

bool FSPickHandler::handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa) {
    /// For some reason, action adapter (aa) does not contain the viewer.
    
    // this may happen while starting the app
//    if (selectionModeCheckBox == NULL) return false;
//    bool add = selectionModeCheckBox->isChecked(); // Adding mode
    bool add = true;
    
    bool end_selection = false;
    if (   ea.getButton()==osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON
           && ea.getEventType()==osgGA::GUIEventAdapter::RELEASE){
        updateRect(_selectionOrigin[0], _selectionOrigin[1]);
        end_selection = true;
    }

    // Pure OSG
//    int shiftPressed = (ea.getModKeyMask()&osgGA::GUIEventAdapter::MODKEY_SHIFT);
    
    // Qt (hackish because I should rather pass the mod key event to OSG even handler.)
    int shiftPressed = QGuiApplication::keyboardModifiers() & Qt::ShiftModifier;
    // handle this event only if the shift key is pressed
    if ( ! shiftPressed){
        // shift not pressed. do not enter this handler
        return false;
    }

    // handle clicking and dragging
    if (
            !(
                    // left-click
                    (   ea.getButton()==osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON )
                    || // drag
                    ea.getEventType()==osgGA::GUIEventAdapter::DRAG
            )
            )
        return false;

    // record the starting point
    bool mouse_push = ea.getEventType()==osgGA::GUIEventAdapter::PUSH;
    if ( mouse_push ){
        _selectionOrigin[0] = ea.getX();
        _selectionOrigin[1] = ea.getY();
    }

    updateRect(ea.getX(), ea.getY());

    osg::ref_ptr<osg::View> view = aa.asView();
    
    if ( view )
    {
        // remove selection rect from the scene (to avoid the rect being chosen as the intersection)

        double xMin, yMin , xMax, yMax;
        if (_selectionOrigin[0] <= ea.getX()){
            xMin = _selectionOrigin[0];
            xMax = ea.getX();
        } else {
            xMin = ea.getX();
            xMax = _selectionOrigin[0];
        }
        if (_selectionOrigin[1] <= ea.getY()){
            yMin = _selectionOrigin[1];
            yMax = ea.getY();
        } else {
            yMin = ea.getY();
            yMax = _selectionOrigin[1];
        }

        // intersection with geometry
        osg::ref_ptr<osgUtil::PolytopeIntersector> intersector =
                new osgUtil::PolytopeIntersector(osgUtil::Intersector::WINDOW,
                                                 xMin, yMin, xMax, yMax);
        osgUtil::IntersectionVisitor iv( intersector.get() );
        iv.setTraversalMask( ~0x1 );
        view->getCamera()->accept( iv );

        selected_material->setAmbient(osg::Material::FRONT_AND_BACK, osg::Vec4 (1.0,0.0,0.0,1.0));
        selected_material->setDiffuse(osg::Material::FRONT_AND_BACK, osg::Vec4 (1.0,1.0,1.0,1.0));
        selected_state->setAttribute(selected_material);
        
        if ( intersector->containsIntersections() ) {
            /// We should in theory remove from selection components no longer selected.
            /// But, it is a bit time-consuming to implement in this code base.
//            const bool forceClear = false;
//            SelectionModel::lastlyCreated_debug->getTemp()->clear(forceClear, this);
            for (auto result = intersector->getIntersections().begin();
                 result != intersector->getIntersections().end(); ++result ){

                osg::ref_ptr<osg::Geometry> geom = result->drawable->asGeometry();

                geom->setStateSet(selected_state);
            }
        }
    }

    if ( end_selection ){
        endSelection();
    }

    // If the user is selecting something, do not pass the events to other handlers (including camera manipulator )
    return true;
}

void FSPickHandler::updateRect(float x, float y) {

    osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array;

    float xOrig, yOrig;
    xOrig = _selectionOrigin[0];
    yOrig = _selectionOrigin[1];

    vertices->push_back(osg::Vec3(xOrig, yOrig, 0.f));
    vertices->push_back(osg::Vec3(x, yOrig, 0.f));
    vertices->push_back(osg::Vec3(x, y, 0.f));
    vertices->push_back(osg::Vec3(xOrig, y, 0.f));

    _selectionRect->setVertexArray(vertices);
}

void FSPickHandler::endSelection() {
    // Hide rect.
    // degenerate rects are undrawn in OpenGL
    updateRect(_selectionOrigin[0], _selectionOrigin[1]);
    
    /// This may happen at the start up of this app
    if (selectionModeCheckBox == NULL) return;
    
    if (selectionModeCheckBox->isChecked()) {
        // add
    } else { // deselect
    }
}

void FSPickHandler::setSelectionModeCheckBox(QCheckBox *selectionModeCheckBox) {
    FSPickHandler::selectionModeCheckBox = selectionModeCheckBox;
}

FSPickHandler::FSPickHandler() {
    getOrCreateSelectionRect();
}

#ifdef BUFFER_OVERFLOW
void FSPickHandler::setSelectionModel(SelectionModel* fsSelection) {
    FSPickHandler::selectionModel = fsSelection;
    std::cerr <<"selectionModel: "<<selectionModel<<" "<<"\t"<< "\x1b[37m" << __FILE__ << ":" << __LINE__ << "\x1b[0m" << std::endl;
}
#endif // BUFFER_OVERFLOW
