//
// Created by SakuraiDaisuke on 8/19/16.
//

#include "vis_util_osg.h"

#include <string>

#include <cassert>
#include <osg/BlendFunc>
#include <osgViewer/Viewer>
#include <osgDB/WriteFile>
#include <osgDB/ReaderWriter>
#include <osgText/Text>
#include <osg/Material>
#include <osgUtil/SmoothingVisitor>

namespace vis_util_osg {

    /**
     * 
     * @param c: ID of component
     * @return 
     */
    osg::Vec4 setColor(int c) {
        switch (c % 3) {
            case 0:
                return osg::Vec4(1.0, 0, 0, 1.0);
            case 1:
                return osg::Vec4(0, 1.0, 0, 1.0);
            case 2:
                return osg::Vec4(0, 0, 1.0, 1.0);
            default:
                break;
        }

        return osg::Vec4(255, 255, 255, 255);
    }

    osg::ref_ptr<osg::Geometry> createNewBox(double *bound) {
        osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
        osg::ref_ptr<osg::Vec3Array> verticesV;
        osg::ref_ptr<osg::DrawElementsUInt> edgeElem;
        osg::ref_ptr<osg::Vec4Array> edgeCol;

        verticesV = new osg::Vec3Array;
        double xMin = bound[0b000];
        double xMax = bound[0b001];
        double yMin = bound[0b010];
        double yMax = bound[0b011];
        double zMin = bound[0b100];
        double zMax = bound[0b101];
        verticesV->push_back(osg::Vec3(xMin, yMin, zMin));
        verticesV->push_back(osg::Vec3(xMax, yMin, zMin));
        verticesV->push_back(osg::Vec3(xMin, yMax, zMin));
        verticesV->push_back(osg::Vec3(xMax, yMax, zMin));
        verticesV->push_back(osg::Vec3(xMin, yMin, zMax));
        verticesV->push_back(osg::Vec3(xMax, yMin, zMax));
        verticesV->push_back(osg::Vec3(xMin, yMax, zMax));
        verticesV->push_back(osg::Vec3(xMax, yMax, zMax));

        edgeElem = new osg::DrawElementsUInt(GL_LINES);

        // only want lines

        // z == 0

        edgeElem->push_back(0b000);
        edgeElem->push_back(0b001);

        edgeElem->push_back(0b000);
        edgeElem->push_back(0b010);

        edgeElem->push_back(0b001);
        edgeElem->push_back(0b011);

        edgeElem->push_back(0b010);
        edgeElem->push_back(0b011);

        // fix (x, y)

        edgeElem->push_back(0b000);
        edgeElem->push_back(0b100);

        edgeElem->push_back(0b001);
        edgeElem->push_back(0b101);

        edgeElem->push_back(0b010);
        edgeElem->push_back(0b110);

        edgeElem->push_back(0b011);
        edgeElem->push_back(0b111);

        // z = 1
        edgeElem->push_back(0b100);
        edgeElem->push_back(0b101);

        edgeElem->push_back(0b100);
        edgeElem->push_back(0b110);

        edgeElem->push_back(0b101);
        edgeElem->push_back(0b111);

        edgeElem->push_back(0b110);
        edgeElem->push_back(0b111);

        // color
        edgeCol = new osg::Vec4Array;
        edgeCol->push_back(osg::Vec4(255, 255, 255, 255));


        geom->setVertexArray(verticesV.get());
        geom->addPrimitiveSet(edgeElem);
        geom->setColorArray(edgeCol);
        geom->setColorBinding(osg::Geometry::BIND_OVERALL);

        return geom;
    }

    std::string serialize(osg::Node &node) {

        std::stringstream sstream;
        osgDB::ReaderWriter *rw = osgDB::Registry::instance()->getReaderWriterForExtension("osg");
        if (rw) rw->writeNode(
                    node,
                    sstream,
                    new osgDB::Options("Ascii")
            );

        return sstream.str();
    }

}

std::ostream& operator<< (std::ostream& o, osg::Node& node) {
    return o << vis_util_osg::serialize(node);
}
