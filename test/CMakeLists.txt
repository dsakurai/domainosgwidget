
add_executable(show_widget
        show_DomainWidget.cpp
)

target_link_libraries(show_widget
    PUBLIC
    DomainOSGWidget
)

add_executable(testDomainOSGWidget
        testDomainOSGWidget.cpp
        )

target_compile_definitions(testDomainOSGWidget
    PRIVATE
        "-DTEST_DIR=${CMAKE_CURRENT_SOURCE_DIR}"
)

target_include_directories(testDomainOSGWidget
        PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/Catch2"
)

target_link_libraries(testDomainOSGWidget
        PUBLIC
        DomainOSGWidget
        )


add_executable(domainviewer
    domainviewer.cpp
)

target_link_libraries(domainviewer
    MainWindow
)
