//
// Created by Daisuke Sakurai on 2/17/19.
//

#include <DomainOSGWidget.h>
#include <vis_util_osg.h>

#include <QApplication>
#include <QSurfaceFormat>

#include <osgDB/ReadFile>

#define CATCH_CONFIG_RUNNER
#include <catch.hpp>

#include <fstream>

#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)

TEST_CASE("Find OpenSceneGraph plugins", "") {
    CHECK(NULL != osgDB::Registry::instance()->getReaderWriterForExtension("osg"));
}

TEST_CASE("serialize", "") {

    double bbox [6] = {-1.0, 1.0, -1.0, 1.0, -1.0, 1.0};

    DomainOSGWidget domainWidget;
    auto box = vis_util_osg::createNewBox(bbox);
    domainWidget.show();

    std::string as_string;
    {
        std::stringstream sstream;
        osgDB::ReaderWriter *rw = osgDB::Registry::instance()->getReaderWriterForExtension("osg");
        if (rw) {
            rw->writeNode(*domainWidget.getView()->getCamera(), sstream, new osgDB::Options("Ascii"));
        }
        as_string = sstream.str();
    }

//    std::ofstream fout (STR(TEST_DIR)"/test.osg");
//    fout << as_string;
//    fout.close();

    std::string expected;

    std::ifstream fin;
    fin.open(STR(TEST_DIR)"/test.osg");

    std::stringstream instream;
    instream << fin.rdbuf();
    fin.close();
    expected = instream.str();

    CHECK(
    as_string == expected
    );
}

int main(int argc, char**argv) {

    QApplication application( argc, argv );

    QSurfaceFormat format;
    format.setVersion(2, 1);
    format.setProfile( QSurfaceFormat::CompatibilityProfile );

    QSurfaceFormat::setDefaultFormat(format);

    const int res = Catch::Session().run(argc, argv);

    return res;
}