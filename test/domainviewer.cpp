//
// Created by Daisuke Sakurai on 2/23/19.
//

//
// Created by Daisuke Sakurai on 2/17/19.
//

#include <MainWindow.h>

#include <QApplication>
#include <QSurfaceFormat>

#include <string>

int main(int argc, char** argv) {

    bool demo = true;

    QApplication application( argc, argv );

    QSurfaceFormat format;
    format.setVersion(2, 1);
    format.setProfile( QSurfaceFormat::CompatibilityProfile );

    QSurfaceFormat::setDefaultFormat(format);

//    DomainOSGWidget domainWidget;
//    domainWidget.addChildToSceneData(osgDB::readNodeFile("cessna.osg"));
//    domainWidget.show();

    MainWindow main_window;

    if (demo) main_window.addMesh("cessna.osg");

    main_window.show();

    return( application.exec() );
}

