//
// Created by Daisuke Sakurai on 2/17/19.
//

#include <DomainOSGWidget.h>

#include <QApplication>
#include <QSurfaceFormat>
#include <osgDB/ReadFile>

int main(int argc, char** argv) {

    QApplication application( argc, argv );

    QSurfaceFormat format;
    format.setVersion(2, 1);
    format.setProfile( QSurfaceFormat::CompatibilityProfile );

    QSurfaceFormat::setDefaultFormat(format);

    DomainOSGWidget domainWidget;
    domainWidget.show();

    return( application.exec() );
}
